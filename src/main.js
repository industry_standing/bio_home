import '@babel/polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/normalize.css'
import './assets/global.css'
import './assets/hover-min.css'
import './assets/animate.css'
import './assets/imagehover.min.css'

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'
import _ from 'lodash'


import dayjs from 'dayjs'
// import 'animate.css'
import {
  WOW
} from 'wowjs'
new WOW({
  live: false
}).init()
Vue.prototype.dayjs = dayjs;
Vue.prototype._ = _
Vue.use(VueCookies);
Vue.use(VueAxios, axios);

import VueLodash from 'vue-lodash'
const options = {
  name: 'lodash'
}
Vue.use(VueLodash, options)


import VueAMap from 'vue-amap';
Vue.use(VueAMap);

VueAMap.initAMapApiLoader({
  key: '010d58cc1b82577d12148d1215e74df1',
  v: '1.4.4'
});

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './element-variables.scss'
Vue.use(ElementUI);

import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload)

var VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo, {
  container: "body",
  duration: 300,
  easing: "ease",
  offset: 0,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
})

import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

Vue.config.productionTip = false;

router.afterEach(() => {
  window.scrollTo(0, 0);
  store.state.cart_show = true;
  store.state.user_location = null;
  store.state.user_location2 = null;
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');