import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    serve : 'https://www.bioflin.com:83',
    // serve  : 'http://api.yelibio.cn',
    top_nav : null,
    cart_count: null,
    cart_show: true,
    user_location: null,
    user_location2: null,
    isLogin: null,
    leftMenuIndex: '',
    adminMenu: [1, 2, 3, 4],
    menu: null,
    login_left:null,
    fexshow:false
  },
  mutations: {
    set_top_nav(state,number){
      state.top_nav = number
    },
    setBread(state, name) {
      state.user_location = name;
    },
    set(state, num) {
      state.cart_count = num;
    },
    changeMenu(state, v) {
      state.adminMenu = v;
    },
    setMenu(state, v) {
      state.menu = v;
    }
  },

})
