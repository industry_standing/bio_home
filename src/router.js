import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)
Vue.use(VueAxios, axios)
const router = new Router({
  routes: [{
      path: '/',
      component: () => import('./views/user/main-tpl.vue'),
      children: [{
          path: '/404',
          component: () => import('./views/user/none/none_404.vue')
        },
        {
          path: '/top',
          component: () => import('./views/user/top.1.vue'),
        },
        {
          path: '/banner_mask',
          component: () => import('./views/user/banner/g_mask.vue'),
        },
        {
          path: '/banner_cellsafe',
          component: () => import('./views/user/banner/g_cellsafe.vue'),
        },
        {
          path: '/static/:id',
          component: () => import('./views/user/static.vue'),
        },

        {
          path: '',
          component: () => import('./views/user/index.1.vue')
        },
        {
          path: 'viewHotNews/:id',
          component: () => import('./views/user/viewHotNews.vue')
        },
        {
          path: 'discoverList',
          component: () => import('./views/user/discoverList.vue')
        },
        {
          path: 'requiredList',
          component: () => import('./views/user/requiredList.vue')
        },
        {
          path: 'newsCenter',
          component: () => import('./views/user/newsCenter.vue')
        },
        {
          path: 'join',
          component: () => import('./views/user/join.vue')
        },
        {
          path: 'bannerMember',
          component: () => import('./views/user/member/banner_member.vue')
        },
        {
          path: 'feedback',
          component: () => import('./views/user/feedback.vue')
        },
        {
          path: 'agreement',
          component: () => import('./views/user/agreement.vue')
        },
        {
          path: 'view2',
          component: () => import('./views/user/view2.vue')
        },
        {
          path: 'test',
          component: () => import('./views/user/index.1.vue')
        },
        {
          path: 'other_login',
          component: () => import('./views/user/other_login.vue')
        },
        {
          path: 'qq_login',
          component: () => import('./views/user/qqLogin.vue')
        },

        {
          path: 'my_login',
          component: () => import('./views/user/my_login.vue')
        },
        {
          path: 'view',
          component: () => import('./views/user/view3.vue')
        },
        {
          path: 'product/:id',
          component: () => import('./views/user/product.vue'),
        },
        {
          path: 'product2/:id/',
          component: () => import('./views/user/product.vue'),
        },
        {
          path: 'news',
          component: () => import('./views/user/bioNews.vue')
        },
        {
          path: 'viewNews',
          component: () => import('./views/user/viewNews.vue')
        },
        {
          path: 'viewRush',
          component: () => import('./views/user/viewRush.vue')
        },
        {
          path: 'viewNews2',
          component: () => import('./views/user/viewNews.vue')
        },
        {
          path: 'results',
          component: () => import('./views/user/results2.vue')
        },
        {
          path: 'forget',
          component: () => import('./views/user/forget.vue')
        },
        {
          path: 'idleHome',
          component: () => import('./views/user/idleHome.vue')
        },
        {
          path: 'patent_index',
          component: () => import('./views/user/patent/patent_index.vue')
        },
        {
          path: 'bioLook_updata',
          component: () => import('./views/user/bioLook/bioLook_updata.vue')
        },
        {
          path: 'bioLook_index',
          component: () => import('./views/user/bioLook/bioLook_index.vue')
        },
        {
          path: 'bioLook_details',
          component: () => import('./views/user/bioLook/bioLook_details.vue')
        },
        {
          path: 'patent_details',
          component: () => import('./views/user/patent/patent_details.vue')
        },
        {
          path: 'share_index',
          component: () => import('./views/user/share/share_index.vue')
        },
        {
          path: 'share_details',
          component: () => import('./views/user/share/share_details.vue')
        },
        {
          path: 'viewIdle',
          component: () => import('./views/user/viewIdle.vue')
        },
        {
          path: 'commend',
          component: () => import('./views/user/commend.vue')
        },
        {
          path: 'brands',
          component: () => import('./views/user/brands.vue')
        },
        {
          path: 'viewBrands',
          component: () => import('./views/user/viewBrands.vue')
        },
        {
          path: 'limit',
          component: () => import('./views/user/limit.vue')
        },
        {
          path: 'hot',
          component: () => import('./views/user/hot.vue')
        },
        {
          path: 'required',
          component: () => import('./views/user/required.vue')
        },
        {
          path: 'group',
          component: () => import('./views/user/group.vue')
        },
        //新团购
        {
          path: 'group_index',
          component: () => import('./views/user/groups/group_index.vue')
        },
        {
          path: 'group_details',
          component: () => import('./views/user/groups/group_details.vue')
        },

        {
          path: 'viewGroup',
          component: () => import('./views/user/viewGroup.vue')
        },
        {
          path: '/shop/shopList',
          component: () => import('./views/user/shop/shopList.vue')
        },
        {
          path: '/shop/shopInfo',
          component: () => import('./views/user/shop/shopInfo.vue')
        },
        {
          path: '/smartSearch',
          component: () => import('./views/user/search/smartSearch.vue')
        },
      ]
    },
    //登陆页和注册页无右侧导航栏
    {
      path: '/',
      component: () => import('./views/user/mainNone.vue'),
      children: [{
          path: 'login',
          component: () => import('./views/user/login.vue')
        },
        {
          path: 'register',
          component: () => import('./views/user/register.vue')
        },
      ]
    },
    //登陆后 无菜单 
    {
      path: '/user',
      component: () => import('./views/user/main-tpl.vue'),
      beforeEnter: (to, from, next) => {
        axios
          .get("https://www.bioflin.com:83/isLogin", {
            // .get("http://api.yelibio.cn/isLogin", {
            params: {
              mobile: VueCookies.get('user'),
              token: VueCookies.get('token'),
            }
          })
          .then(res => {
            if (res.data.data == 1) {
              next();
            } else {
              next('/login');
            }
          });
      },
      children: [{
          path: 'try_use',
          component: () => import('./views/user/try_use.vue')
        },
        // {
        //   path: 'order',
        //   component: () => import('./views/user/order.vue')
        // },
        {
          path: 'order3',
          component: () => import('./views/user/order3.vue')
        },
        {
          path: 'order4',
          name: 'order4',
          component: () => import('./views/user/order4.vue')
        },
        {
          path: 'joinGroupOrder',
          component: () => import('./views/user/joinGroupOrder.vue')
        },
        {
          path: 'groupOrder',
          component: () => import('./views/user/groupOrder.vue')
        },
        {
          path: 'order2',
          component: () => import('./views/user/order2.vue')
        },
        {
          path: 'viewPoint',
          component: () => import('./views/user/viewPoint.vue')
        },
        {
          path: 'points',
          component: () => import('./views/user/points.vue')
        },

        {
          path: 'couponCenter',
          component: () => import('./views/user/couponCenter.vue')
        },
        {
          path: 'createOrder',
          component: () => import('./views/user/createOrder.vue')
        },
        {
          path: 'createIdleOrder',
          component: () => import('./views/user/createIdleOrder.vue')
        },
        {
          path: 'pay',
          component: () => import('./views/user/pay.vue')
        },
        {
          path: 'pay2',
          component: () => import('./views/user/pay2.vue')
        },
        // {
        //   path: 'cart',
        //   component: () => import('./views/user/cart.vue')
        // },
        {
          path: 'cart',
          component: () => import('./views/user/cart2.vue')
        },
        {
          path: 'finish1',
          component: () => import('./views/user/finish1.vue')
        },
        {
          path: 'weChatPayQRcode',
          component: () => import('./views/user/weChatPayQRcode.vue')
        },
        {
          path: 'createOrder1',
          component: () => import('./views/user/createOrder.1.vue')
        },
        {
          path: 'finish_1',
          component: () => import('./views/user/finish_1.vue')
        },

      ]
    },

    //登陆后有左菜单
    {
      path: '/user',
      component: () => import('./views/user/loginedTpl.vue'),
      beforeEnter: (to, from, next) => {
        axios
          .get("https://www.bioflin.com:83/isLogin", {
            // .get("http://api.yelibio.cn/isLogin", {
            params: {
              mobile: VueCookies.get('user'),
              token: VueCookies.get('token'),
            }
          })
          .then(res => {
            if (res.data.data == 1) {
              next();
            } else {
              next('/login');
            }
          });
      },
      children: [{
          path: 'viewState/:id',
          component: () => import('./views/user/viewState.vue'),
        },
        {
          path: 'state/:tab/:path',
          component: () => import('./views/user/state.vue')
        },
        {
          path: 'policy/:type/:path',
          component: () => import('./views/user/policy.vue'),
        },
        {
          path: 'riskManage/:tab/:path',
          component: () => import('./views/user/riskManage.vue')
        },
        {
          path: 'login_share/:path',
          component: () => import('./views/user/share/login_share.vue')
        },
        {
          path: 'createPatent/:path',
          component: () => import('./views/user/patent/createPatent.vue')
        },
        {
          path: 'harmful_detail/:id',
          component: () => import('./views/user/harmful_detail.vue'),
        },

        {
          path: 'state_detail/:id',
          component: () => import('./views/user/state_detail.vue')
        },
        {
          path: 'viewEx/:id',
          component: () => import('./views/user/viewEx.vue')
        },
        {
          path: 'manage/:id',
          component: () => import('./views/user/manage.vue')
        },
        {
          path: 'expenditure/:id/:path',
          component: () => import('./views/user/expenditure.vue')
        },
        {
          path: 'recycle',
          component: () => import('./views/user/recycle.vue')
        },
        {
          path: 'orders/:path',
          component: () => import('./views/user/orders.vue')
        },
        {
          path: 'orders1/:path',
          component: () => import('./views/user/my/newOrders.vue')
        },
        {
          path: 'viewOrder/:path',
          component: () => import('./views/user/viewOrder.vue')
        },
        {
          path: 'address/:path',
          component: () => import('./views/user/address.vue')
        },
        {
          path: 'collections/:path',
          component: () => import('./views/user/collections.vue')
        },
        {
          path: 'like',
          component: () => import('./views/user/like.vue')
        },
        {
          path: 'userInfo/:path',
          component: () => import('./views/user/userInfo.vue')
        },
        {
          path: 'safety/:path',
          component: () => import('./views/user/safety.vue')
        },
        {
          path: 'bindMobile',
          component: () => import('./views/user/bindMobile.vue')
        },
        {
          path: 'bindEmail',
          component: () => import('./views/user/bindEmail.vue')
        },
        {
          path: 'messages/:path',
          component: () => import('./views/user/message.vue')
        },
        {
          path: 'coupons/:path',
          component: () => import('./views/user/coupons.vue')
        },
        {
          path: 'history/:path',
          component: () => import('./views/user/history.vue')
        },
        {
          path: 'projects',
          component: () => import('./views/user/projects.vue')
        },
        {
          path: 'viewProject',
          component: () => import('./views/user/viewProject.vue')
        },
        {
          path: 'store',
          component: () => import('./views/user/store.vue')
        },
        {
          path: 'children/:path',
          component: () => import('./views/user/children.vue')
        },
        {
          path: 'score/:path',
          component: () => import('./views/user/score.vue')
        },
        {
          path: 'viewChild',
          component: () => import('./views/user/viewChild.vue')
        },

        {
          path: 'idle/:path',
          component: () => import('./views/user/idle.vue')
        },
        {
          path: 'idlePublish',
          component: () => import('./views/user/idlePublish.vue')
        },
        {
          path: 'comment',
          component: () => import('./views/user/comment.vue')
        },
        {
          path: 'comment/after',
          component: () => import('./views/user/commentAfter.vue')
        },
        {
          path: 'idle/after',
          component: () => import('./views/user/idlePublishAfter.vue')
        },
        {
          path: 'after/:path',
          component: () => import('./views/user/after.vue')
        },
        {
          path: 'afterForm',
          component: () => import('./views/user/afterForm.vue')
        },
        {
          path: 'invit/:path',
          component: () => import('./views/user/invit.vue')
        },
        {
          path: 'brand/:path',
          component: () => import('./views/user/brand2.vue')
        },
        {
          path: 'viewBrand',
          component: () => import('./views/user/viewBrand.vue')
        },
        {
          path: 'myNews/:tab/:path',
          component: () => import('./views/user/myNews.vue')
        },
        {
          path: 'state/:tab/:path',
          component: () => import('./views/user/state.vue')
        },
        {
          path: 'policy/:type/:path',
          component: () => import('./views/user/policy.vue'),
        },
        {
          path: 'riskManage/:tab/:path',
          component: () => import('./views/user/riskManage.vue')
        },
        {
          path: 'harmful_detail/:id',
          component: () => import('./views/user/harmful_detail.vue'),
        },
        {
          path: 'state_detail/:id',
          component: () => import('./views/user/state_detail.vue')
        },
        {
          path: 'viewEx/:id',
          component: () => import('./views/user/viewEx.vue')
        },
        {
          path: 'manage/:id',
          component: () => import('./views/user/manage.vue')
        },
        {
          path: 'expenditure/:id/:path',
          component: () => import('./views/user/expenditure.vue')
        },
        {
          path: 'recycle',
          component: () => import('./views/user/recycle.vue')
        },
        {
          path: 'home/:path',
          component: () => import('./views/user/home.vue')
        },

        {
          path: 'viewOrder',
          component: () => import('./views/user/viewOrder.vue')
        },
        {
          path: 'address/:path',
          component: () => import('./views/user/address.vue')
        },
        {
          path: 'collections/:path',
          component: () => import('./views/user/collections.vue')
        },
        {
          path: 'like',
          component: () => import('./views/user/like.vue')
        },
        {
          path: 'userInfo/:path',
          component: () => import('./views/user/userInfo.vue')
        },
        {
          path: 'safety/:path',
          component: () => import('./views/user/safety.vue')
        },
        {
          path: 'bindMobile',
          component: () => import('./views/user/bindMobile.vue')
        },
        {
          path: 'bindEmail',
          component: () => import('./views/user/bindEmail.vue')
        },
        {
          path: 'messages/:path',
          component: () => import('./views/user/message.vue')
        },
        {
          path: 'bargain/:path',
          component: () => import('./views/user/member/bargain.vue'),
        },
        {
          path: 'coupons/:path',
          component: () => import('./views/user/coupons.vue')
        },
        {
          path: 'history/:path',
          component: () => import('./views/user/history.vue')
        },
        {
          path: 'projects',
          component: () => import('./views/user/projects.vue')
        },
        {
          path: 'viewProject',
          component: () => import('./views/user/viewProject.vue')
        },
        {
          path: 'store',
          component: () => import('./views/user/store.vue')
        },
        {
          path: 'children/:path',
          component: () => import('./views/user/children.vue')
        },
        {
          path: 'childrenOrder/:path',

          component: () => import('./views/user/my/childrenOrder.vue')
        },
        {
          path: 'score/:path',
          component: () => import('./views/user/score.vue')
        },
        {
          path: 'viewChild',
          component: () => import('./views/user/viewChild.vue')
        },
        {
          path: 'idle/:path',
          component: () => import('./views/user/idle.vue')
        },
        {
          path: 'idlePublish',
          component: () => import('./views/user/idlePublish.vue')
        },
        {
          path: 'comment',
          component: () => import('./views/user/comment.vue')
        },
        {
          path: 'comment/after',
          component: () => import('./views/user/commentAfter.vue')
        },
        {
          path: 'idle/after',
          component: () => import('./views/user/idlePublishAfter.vue')
        },
        {
          path: 'after/:path',
          component: () => import('./views/user/after.vue')
        },
        {
          path: 'viewAfter/:path',
          component: () => import('./views/user/viewAfter.vue')
        },
        {
          path: 'afterForm',
          component: () => import('./views/user/afterForm.vue')
        },
        {
          path: 'invit/:path',
          component: () => import('./views/user/invit.vue')
        },
        {
          path: 'brand/:path',
          component: () => import('./views/user/brand2.vue')
        },
        {
          path: 'viewBrand',
          component: () => import('./views/user/viewBrand.vue')
        },
        {
          path: 'myNews/:tab/:path',
          component: () => import('./views/user/myNews.vue')
        }, {
          path: 'refund/:path',
          component: () => import('./views/user/refund/refund.vue')
        },

      ]
    },
    {
      path: '/help',
      component: () => import('./views/user/help/noMenuTpl.vue'),
      children: [{
          path: '8',
          // component: () => import('./views/user/help/help1_1.vue'),
          components: {
            default: () => import('./views/user/help/help1_1.vue'),
          },
        },
        {
          path: '1_2',
          components: {
            default: () => import('./views/user/help/help1_2.vue'),
          },
        },
        {
          path: '1_3',
          components: {
            default: () => import('./views/user/help/help1_3.vue'),
          },
        },
        {
          path: '1_4',
          components: {
            default: () => import('./views/user/help/help1_4.vue'),
          },
        },
        {
          path: '2_1',
          components: {
            default: () => import('./views/user/help/help2_1.vue'),
          },
        },
        {
          path: '2_2',
          components: {
            default: () => import('./views/user/help/help2_2.vue'),
          },
        },
        {
          path: '2_3',
          components: {
            default: () => import('./views/user/help/help2_3.vue'),
          },
        },
        {
          path: '2_4',
          components: {
            default: () => import('./views/user/help/help2_4.vue'),
          },
        },
        {
          path: '3_1',
          components: {
            default: () => import('./views/user/help/help3_1.vue'),
          },
        },
        {
          path: '3_2',
          components: {
            default: () => import('./views/user/help/help3_2.vue'),
          },
        },
        {
          path: '3_3',
          components: {
            default: () => import('./views/user/help/help3_3.vue'),
          },
        },
        {
          path: '3_4',
          components: {
            default: () => import('./views/user/help/help3_4.vue'),
          },
        },
        {
          path: '4_1',
          components: {
            default: () => import('./views/user/help/help4_1.vue'),
          },
        },
        {
          path: '4_2',
          components: {
            default: () => import('./views/user/help/help4_2.vue'),
          },
        },
        {
          path: '4_3',
          components: {
            default: () => import('./views/user/help/help4_3.vue'),
          },
        },
        {
          path: '4_4',
          components: {
            default: () => import('./views/user/help/help4_4.vue'),
          },
        },
        {
          path: '5_1',
          components: {
            default: () => import('./views/user/help/help5_1.vue'),
          },
        },
        {
          path: '5_2',
          components: {
            default: () => import('./views/user/help/help5_2.vue'),
          },
        },
        {
          path: '6_1',
          components: {
            default: () => import('./views/user/help/help6_1.vue'),
          },
        },
        {
          path: '6_2',
          components: {
            default: () => import('./views/user/help/help6_2.vue'),
          },
        },
        {
          path: '6_3',
          components: {
            default: () => import('./views/user/help/help6_3.vue'),
          },
        },
        {
          path: '6_4',
          components: {
            default: () => import('./views/user/help/help6_4.vue'),
          },
        },
        {
          path: '7_1',
          components: {
            default: () => import('./views/user/help/help7_1.vue'),
          },
        },
        {
          path: '7_2',
          components: {
            default: () => import('./views/user/help/help7_2.vue'),
          },
        },
        {
          path: '7_3',
          components: {
            default: () => import('./views/user/help/help7_3.vue'),
          },
        },
        {
          path: '7_3',
          components: {
            default: () => import('./views/user/help/help7_3.vue'),
          },
        },
        {
          path: '7_3',
          components: {
            default: () => import('./views/user/help/help7_3.vue'),
          },
        },
        {
          path: '9',
          components: {
            default: () => import('./views/user/help/help9.vue'),
          },
        },
        {
          path: '10',
          components: {
            default: () => import('./views/user/help/help10.vue'),
          },
        },
        {
          path: '11',
          components: {
            default: () => import('./views/user/help/help11.vue'),
          },
        },

        {
          path: '12',
          components: {
            default: () => import('./views/user/help/help12.vue'),
          },
        },

      ]
    },
    {
      path: '*', // 页面不存在的情况下会跳到404页面
      redirect: '/404',
      name: 'notFound',
      hidden: true
    }
  ]

})
export default router